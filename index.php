<?php
	include 'inc/header.php';
	include 'lib/User.php';
?>

<?php
	Session::checkSession();
?>

<?php
	$user = new User();
?>
	
<?php
	$loginMessage = Session::get("loginMessage");
	if (isset($loginMessage)) {
		echo $loginMessage;
	}
	Session::set('loginMessage', NULL);
?>

<div class="panel panel-default">
	<!-- 
  	<div class="panel-heading">
      <h3 class="panel-title">Panel title</h3>
    </div>
    -->
	<div class="panel-body">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<span class="navbar-brand"><h4>Digital Classroom</h4></span>
				</div> <!-- 'navbar-header' collapse  -->
				<ul class="nav navbar-nav pull-right">
					<li><a><h4>Join Classroom</h4></a></li>
				</ul>
			</div> <!-- 'container-fluid' collapse  -->
		</nav>

		<div class="row">		
				<div class="col-md-4">
			      <div class="thumbnail">
			        <a href="/w3images/lights.jpg" target="_blank">
			          <img src="inc/img/img1.png" alt="Lights" style="width:100%; height:110px;">
			          <div class="caption">
			            <h3>System Analysis & Design</h3>
			          </div>
			        </a>
			      </div>
			    </div>

			    <div class="col-md-4">
			      <div class="thumbnail">
			        <a href="/w3images/lights.jpg" target="_blank">
			          <img src="inc/img/img2.jpg" alt="Lights" style="width:100%; height:110px;">
			          <div class="caption">
			            <h3 style="text-align:center;">Software Security</h3>
			          </div>
			        </a>
			      </div>
			    </div>

			    <div class="col-md-4">
			      <div class="thumbnail">
			        <a href="/w3images/lights.jpg" target="_blank">
			          <img src="inc/img/img3.jpg" alt="Lights" style="width:100%; height:110px;">
			          <div class="caption">
			            <h3 style="text-align:center;">	Programming Language</h3>
			          </div>
			        </a>
			      </div>
			    </div>

			    <div class="col-md-4">
			      <div class="thumbnail">
			        <a href="/w3images/lights.jpg" target="_blank">
			          <img src="inc/img/img4.png" alt="Lights" style="width:100%; height:110px;">
			          <div class="caption">
			            <h3 style="text-align:center;">Theory of Computing</h3>
			          </div>
			        </a>
			      </div>
			    </div>


			    <div class="col-md-4">
			      <div class="thumbnail">
			        <a href="/w3images/lights.jpg" target="_blank">
			          <img src="inc/img/img7.jpg" alt="Lights" style="width:100%; height:110px;">
			          <div class="caption">
			            <h3 style="text-align:center;">Introduction to Algorithm</h3>
			          </div>
			        </a>
			      </div>
			    </div>
		    
		</div> <!-- 'row' collapse -->

	</div> <!-- 'panel-body' collapse -->
</div> <!-- 'panel panel-default' collapse -->

<?php include 'inc/footer.php'; ?>
