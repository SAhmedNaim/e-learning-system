<?php
	include 'inc/header.php';
	include 'lib/User.php';
?>

<?php
	Session::checkSession();
?>

<?php
	$user = new User();
?>

<div class="panel panel-default">
	<!-- 
  	<div class="panel-heading">
      <h3 class="panel-title">Panel title</h3>
    </div>
    -->
	<div class="panel-body">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<span class="navbar-brand"><h4>Online Exam</h4></span>
				</div> <!-- 'navbar-header' collapse  -->
				<ul class="nav navbar-nav pull-right">
					<li><a><h4>Time: 02:00 Hours</h4></a></li>
				</ul>
			</div> <!-- 'container-fluid' collapse  -->
		</nav>



<form action="">

<!-- exam ques 1 -->
<div class="panel panel-default">
  <div class="panel-heading"><h3>1. Who is the president of U.S.A?</h3></div>
  <div class="panel-body">
    <a class="list-group-item list-group-item-default">
    	<label><input type="checkbox" name="checkbox" value="value"> Sakib Khan</label>
    </a>
  	<a class="list-group-item list-group-item-default">
		<label><input type="checkbox" name="checkbox" value="value"> Justin Bieber</label>
  	</a>
  	<a class="list-group-item list-group-item-default">
		<label><input type="checkbox" name="checkbox" value="value"> Donald Trump</label>
  	</a>
  	<a class="list-group-item list-group-item-default">
		<label><input type="checkbox" name="checkbox" value="value"> Rabeya Jannat</label>
  	</a>
  </div>
</div>


<!-- exam ques 2 -->
<div class="panel panel-default">
  <div class="panel-heading"><h3>2. Who is the president of U.S.A?</h3></div>
  <div class="panel-body">
    <a class="list-group-item list-group-item-default">
    	<label><input type="checkbox" name="checkbox" value="value"> Sakib Khan</label>
    </a>
  	<a class="list-group-item list-group-item-default">
		<label><input type="checkbox" name="checkbox" value="value"> Justin Bieber</label>
  	</a>
  	<a class="list-group-item list-group-item-default">
		<label><input type="checkbox" name="checkbox" value="value"> Donald Trump</label>
  	</a>
  	<a class="list-group-item list-group-item-default">
		<label><input type="checkbox" name="checkbox" value="value"> Rabeya Jannat</label>
  	</a>
  </div>
</div>



<!-- exam ques 3 -->
<div class="panel panel-default">
  <div class="panel-heading"><h3>3. Who is the president of U.S.A?</h3></div>
  <div class="panel-body">
    <a class="list-group-item list-group-item-default">
    	<label><input type="checkbox" name="checkbox" value="value"> Sakib Khan</label>
    </a>
  	<a class="list-group-item list-group-item-default">
		<label><input type="checkbox" name="checkbox" value="value"> Justin Bieber</label>
  	</a>
  	<a class="list-group-item list-group-item-default">
		<label><input type="checkbox" name="checkbox" value="value"> Donald Trump</label>
  	</a>
  	<a class="list-group-item list-group-item-default">
		<label><input type="checkbox" name="checkbox" value="value"> Rabeya Jannat</label>
  	</a>
  </div>
</div>
	<div class="col-md-4"></div>
	<div class="col-md-4">
    	<input style="min-width:100%;" type="submit" value="Submit" class="btn btn-default"/>
  	</div>
  	<div class="col-md-4"></div>
</form>


	</div> <!-- 'panel-body' collapse -->
</div> <!-- 'panel panel-default' collapse -->

<?php include 'inc/footer.php'; ?>