  <!-- 
  <div class="panel-footer">
  	<h4 style="text-align:center;">
  		&copy; Copyright: 2017 - All rights reserved
  	</h4>
  </div>
  -->

  <div class="well">
		<h3>
			&copy; Copyright: <?php echo date('Y'); ?>
			<span class="pull-right">All rights reserved.</span>
		</h3>
	</div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="inc/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="inc/js/bootstrap.min.js"></script>
  </body>
</html>
