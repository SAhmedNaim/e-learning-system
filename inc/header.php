<?php
    $filepath = realpath(dirname(__FILE__));
    include_once $filepath.'/../lib/Session.php';
    Session::initialize();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>E-learning System</title>

    <!-- Bootstrap -->
    <link href="inc/css/bootstrap.min.css" rel="stylesheet"/>

    <!-- custon css -->
    <link href="inc/css/custom.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

<?php
    if (isset($_GET['action']) && $_GET['action'] == 'logout') {
        Session::destroy();
    }

?>

  <body>
    
 <div class="container">
      


<!-- default navbar code goes here -->
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php"><b>E-Learning System</b></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="blog.php">Blog <span class="sr-only">(current)</span></a></li>
        
        <!--
        <li><a href="#">One more link goes here </a></li>
        -->

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Programming Language <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">PHP</a></li>
            <li><a href="#">C#.NET</a></li>
            <li><a href="#">ASP.NET</a></li>
            <!-- 
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">One more separated link</a></li>
            -->
          </ul>
        </li>
      </ul>

      <!-- default searchbar 
      <form class="navbar-form navbar-left">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      -->
      
      <ul class="nav navbar-nav navbar-right">

<?php 
    $id = Session::get('id');
    $login = Session::get('login');
    $status = Session::get('status');
    if ($login == true) {
?>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <?php 
                $username = Session::get('username');
                if (isset($username)) {
                    echo $username;
                }
            ?> 
            <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <!-- 
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            -->
            <li><a href="profile.php?id=<?php echo $id; ?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Profile</a></li>
            
      <?php if ($status == 'teacher' OR $status == 'admin') { ?>
              <li><a href="approve.php">
                  <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span> Approval Setting</a>
              </li>
      <?php } ?>

            <li role="separator" class="divider"></li>

            <li><a href="?action=logout">
                <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Log out</a>
            </li>
          </ul>
        </li>
<?php 
    } else {
?>
        <li><a href="login.php">Login</a></li>
        <li><a href="register.php">Registration</a>
<?php 
    }
?>
      <!-- pending notification
      <li role="presentation"><a href="#">Messages <span class="badge">3</span></a></li>
      -->

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<!-- default navbar just collapse -->
