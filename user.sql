-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2017 at 12:45 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elearning`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `active` int(11) NOT NULL,
  `course` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `status`, `active`, `course`, `password`) VALUES
(1, 'Naim', 'naim.ahmed035@gmail.com', 'student', 1, 'syetemAnalysis', '12345'),
(2, 'Naim', 'naim.ahmed005@gmail.com', 'student', 0, 'syetemAnalysis', '12345'),
(3, 'Alamgir', 'alamgir@gmail.com', 'teacher', 1, 'syetemAnalysis', '12345'),
(4, 'Shakil', 'shakil@gmail.com', 'student', 1, 'syetemAnalysis', '12345'),
(9, 'Hariz', 'hariz@gmail.com', 'student', 1, 'softwareSecurity', '12345'),
(5, 'Nasir', 'nasir@yahoo.com', 'student', 0, 'system analysis', '12345'),
(6, 'Nobin', 'nobin@gmail.com', 'teacher', 0, 'system analysis', '12345'),
(7, 'Ali', 'ali@gmail.com', 'student', 2, 'software security', '12345'),
(8, 'Admin', 'admin@gmail.com', 'admin', 1, '', '12345');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
