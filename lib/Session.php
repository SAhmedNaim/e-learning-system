<?php
	
class Session {
	public static function initialize() {
		if (version_compare(phpversion(), '5.4.0', '<')) {
			if (session_id() == '') {
				session_start();
			}
		} else {
			if (session_status() == PHP_SESSION_NONE) {
				session_start();
			}
		}
	}

	public static function set($name, $value) {
		$_SESSION[$name] = $value;
	}

	public static function get($name) {
		if (isset($_SESSION[$name])) {
			return $_SESSION[$name];
		} else {
			return false;
		}
	}

	public static function checkSession() {
		if (self::get('login') == false) {
			self::destroy();
			header('Location:login.php');
		}
	}

	public static function checkLogin() {
		if (self::get('login') == true) {
			header('Location:index.php');
		}
	}

	public static function destroy() {
		session_destroy();
		session_unset();
		header('Location:login.php');
	}

}

?>