<?php
	$filepath = realpath(dirname(__FILE__));
	include_once($filepath.'/../config/config.php');
?>

<?php

class Database {
	private $hostdb = DB_HOST;
 	private $userdb = DB_USER;
 	private $passdb = DB_PASS;
 	private $namedb	= DB_NAME;
 	public $pdo;

 	public function __construct() {
 		$this->connectDB();
 	}

 	private function connectDB() {
 		if(!isset($this->pdo)) {
 			try {
 				$link = new PDO("mysql:host=".$this->hostdb.";dbname=".$this->namedb, $this->userdb, $this->passdb);
 				$link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 				$link->exec("SET CHARACTER SET utf8");
 				$this->pdo = $link;
 				return $this->pdo;
 			} catch (PDOException $e) {
 				die("Failed to connect with Database. ".$e->getMessage());
 			}
 		}
 	}

 	


}

?>