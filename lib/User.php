<?php
	include_once 'Session.php';
	include 'Database.php';
?>

<?php

class User {
	private $db;
	
	public function __construct() {
		$this->db = new Database();
	}

	public function userRegistration($data) {
		// retrieve all data using POST method from registration page
		$username	= $data['username'];
		$email 		= $data['email'];
		if(!isset($data['status'])) {
			$status = "";
		} else {
			$status = $data['status'];
		}
		$active = 0;
		if(!isset($data['course'])) {
			$course = "";
		} else {
			$course = $data['course'];
		}
		$password 	= $data['password'];

		// checkEmail() function called to validate email
		$check_email = $this->checkEmail($email);

		// empty field validation
		if ($username == "" OR $email == "" OR $status == "" OR $course == "" OR $password == "") {
			$msg = "<div class='alert alert-danger alert-dismissable fade in'>
					<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
					<strong>Error! </strong>Field must not be empty.</div>";
			return $msg;
		}

		// username length validation
		if (strlen($username) < 3) {
			$msg = "<div class='alert alert-danger alert-dismissable fade in'>
					<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
					<strong>Error! </strong>Username must not be less than or equal to 3 characters.</div>";
			return $msg;
		} elseif (preg_match('/[^a-z0-9_-]+/i', $username)) {
			$msg = "<div class='alert alert-danger alert-dismissable fade in'>
					<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
					<strong>Error! </strong>Username only contains alphanumerical, dashes and underscores.</div>";
			return $msg;
		}

		// password length validation
		if (strlen($password) < 3) {
			$msg = "<div class='alert alert-danger alert-dismissable fade in'>
					<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
					<strong>Error! </strong>Password must not be less than or equal to 3 characters.</div>";
			return $msg;
		} 

		// email validation
		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
			$msg = "<div class='alert alert-danger alert-dismissable fade in'>
					<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
					<strong>Error! </strong>Email address is not valid.</div>";
			return $msg;
		}

		// email repeatation validate
		if ($check_email == true) {
			$msg = "<div class='alert alert-danger alert-dismissable fade in'>
					<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
					<strong>Error! </strong>Email address already exists.</div>";
			return $msg;
		}

		// send data to user table
		$sql = "INSERT INTO user (username, email, status, active, course, password) VALUES(:username, :email, :status, :active, :course, :password)";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(":username", $username);
		$query->bindValue(":email", $email);
		$query->bindValue(":status", $status);
		$query->bindValue(":active", $active);
		$query->bindValue(":course", $course);
		$query->bindValue(":password", $password);
		$result = $query->execute();
                

		if ($result) {
			$msg = "<div class='alert alert-info alert-dismissable fade in'>
					<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
					<strong>Thank you! </strong>Your form has been submitted successfully.</div>";
			return $msg;
		} else {
			$msg = "<div class='alert alert-danger alert-dismissable fade in'>
					<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
					<strong>Error! </strong> Something is going to be wrong.</div>";
			return $msg;
		}

	}

	// check email existance at user table
	public function checkEmail($email) {
		$sql = "SELECT email FROM user WHERE email = :email";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':email', $email);
		$query->execute();
		if ($query->rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	}

	// check user wheather it would active or not
	public function checkActive($email) {
		$sql = "SELECT active FROM user WHERE email = :email";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':email', $email);
		$query->execute();
		$result	= $query->fetchColumn();
		return $result;
	}

	// retrieve email and password to login user
	public function getLoginUser($email, $password) {
		$sql = "SELECT * FROM user WHERE email = :email AND password = :password LIMIT 1";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':email', $email);
		$query->bindValue(':password', $password);
		$query->execute();
		$result = $query->fetch(PDO::FETCH_OBJ);
		return $result;
	}

	// method for user login into system
	public function userLogin($data) {
		// retrieve data from login page
		$email 		= $data['email'];
		$password 	= $data['password'];

		// checkEmail() function is called to validate email
		$check_email = $this->checkEmail($email);

		// check_active() function is called to check an user
		$check_active = $this->checkActive($email);

		// validating fields wheres fields are empty or not
		if ($email == "" OR $password == "") {
			$msg = "<div class='alert alert-danger alert-dismissable fade in'>
					<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
					<strong>Error! </strong>Field must not be empty.</div>";
			return $msg;
		}

		// email validation
		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
			$msg = "<div class='alert alert-danger alert-dismissable fade in'>
					<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
					<strong>Error! </strong>Email address is not valid.</div>";
			return $msg;
		}

		if ($check_email == true) {
			if($check_active == 0) {
				$msg = "<div class='alert alert-danger alert-dismissable fade in'>
						<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
						<strong>Sorry! </strong>Your registration is pending yet.</div>";
				return $msg;
			} elseif ($check_active == 1) {
				/*
				$msg = "<div class='alert alert-danger alert-dismissable fade in'>
						<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
						<strong>Error! </strong>You are active user.</div>";
				return $msg;
				*/
				$result = $this->getLoginUser($email, $password);
				if ($result) {
					Session::initialize();
					Session::set('login', true);
					Session::set('id', $result->id);
					Session::set('username', $result->username);
					Session::set('email', $result->email);
					Session::set('status', $result->status);
					Session::set('course', $result->course);
					Session::set('loginMessage', "<div class='alert alert-success alert-dismissable fade in'>
						<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
						<strong>Congratulation! </strong>You have already logged in to our system.</div>");
					header('Location: index.php');
				} /*else {
					$msg = "<div class='alert alert-danger alert-dismissable fade in'>
						<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
						<strong>Error! </strong>Data not found.</div>";
					return $msg;
				}*/
			} elseif($check_active == 2) {
				$msg = "<div class='alert alert-danger alert-dismissable fade in'>
						<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
						<strong>Error! </strong>You are already rejected.</div>";
				return $msg;
			} 
		} else {
			$msg = "<div class='alert alert-danger alert-dismissable fade in'>
					<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
					<strong>Sorry! </strong>Please sign up to continue.</div>";
			return $msg;
		}


	}

	public function getStudentData() {
		$status = Session::get('status');
 		$sql = "SELECT * FROM `user` WHERE STATUS = :status";
 		$query = $this->db->pdo->prepare($sql);
 		if ($status == 'admin') {
 			$query->bindValue(':status', 'teacher');
 		} elseif ($status == 'teacher') {
 			$query->bindValue(':status', 'student');
 		}
 		$query->execute();
 		$result = $query->fetchAll();
 		return $result;
 	}

 	public function approveByID($id) {
 		$sql = "UPDATE user SET `active` = 1 WHERE id = :id";
 		$query = $this->db->pdo->prepare($sql);
 		$query->bindParam(':id', $id);
 		$query->execute();
 		echo("<script>location.href = 'approve.php';</script>");
 	}

 	public function rejectByID($id) {
 		$sql = "UPDATE user SET `active` = 2 WHERE id = :id";
 		$query = $this->db->pdo->prepare($sql);
 		$query->bindParam(':id', $id);
 		$query->execute();
 		echo("<script>location.href = 'approve.php';</script>");
 	}








}

?>