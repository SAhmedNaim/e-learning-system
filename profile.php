<?php include 'inc/header.php'; ?>

<?php
	Session::checkSession();
?>


<div class="panel panel-default">
	<div class="panel-body">
		<!-- default navbar goes here -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<span class="navbar-brand"><h4>Profile</h4></span>
				</div>
				<ul class="nav navbar-nav pull-right">
					<li><a><h4><?php echo $username; ?></h4></a></li>
				</ul>
			</div>
		</nav>
		
<!-- information table goes here -->
<form>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
  </div>
  <div class="form-group">
    <label for="exampleInputFile">File input</label>
    <input type="file" id="exampleInputFile">
    <p class="help-block">Example block-level help text here.</p>
  </div>
  <div class="checkbox">
    <label>
      <input type="checkbox"> Check me out
    </label>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
	</div>
</div>


<?php
	include 'inc/footer.php';
?>