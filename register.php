<?php 
	include 'inc/header.php'; 
	include 'lib/User.php';
?>

<?php
	$user = new User();
	if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['register'])) {
		$userRegistration = $user->userRegistration($_POST);
	}
?>

<div class="panel panel-default">

<div class="panel-heading">
    <h2>User Login</h2>
</div>
<div class="panel-body">
    <div style="max-width: 600px; margin: 0px auto;">

<?php
	if (isset($userRegistration)) {
		echo $userRegistration;
	}
?>
      	<form action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
			<!-- username -->
			<div class="form-group">
				<label for="username">Username</label>
				<input type="text" id="username" name="username" class="form-control" placeholder="Type your username..." style="min-width:100%" />
			</div>

			<!-- password -->
			<div class="form-group">
				<label for="email">Email Address</label>
				<input type="text" id="email" name="email" class="form-control" placeholder="Type your email address..." style="min-width:100%" />
			</div>
			
			<!-- status -->
			<label for="status">Status</label>
			<select name="status" class="form-control">
				<option disabled="" selected="">Choose your status</option>
			  	<option value="student">Student</option>
			  	<option value="teacher">Teacher</option>
			</select>
			<br/>

			<!-- course -->
			<label for="course">Course</label>
			<select name="course" class="form-control">
				<option disabled="" selected="">Select your course</option>
			  	<option value="syetemAnalysis">System Analysis</option>
			  	<option value="programmingLanguage">Programming Langugage</option>
			  	<option value="softwareSecurity">Software Security</option>
			  	<option value="dataStructure">Data Structure</option>
			  	<option value="algorithm">Algorithm</option>
			</select>
			<br/>

			<!-- password -->
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" id="password" name="password" class="form-control" placeholder="Type a password here..." style="min-width:100%" />
			</div>
			<button type="submit" name="register" class="btn btn-success">Submit</button>
		</form>
    </div>
</div>

</div>

<?php include 'inc/footer.php'; ?>