<?php include 'inc/header.php'; ?>

<?php 
	include 'lib/User.php'; 
	$user = new User();
?>

<?php Session::checkSession(); ?>

<?php
	if (isset($_GET['action']) && $_GET['action'] == 'approve') {
		$id = (int)$_GET['id'];
		$approve = $user->approveByID($id);
	}
?>

<?php
	if (isset($_GET['action']) && $_GET['action'] == 'reject') {
		$id = (int)$_GET['id'];
		$reject = $user->rejectByID($id);
	}
?>

<div class="panel panel-default">
	<div class="panel-body">
		<!-- default navbar goes here -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<span class="navbar-brand"><h4>Profile</h4></span>
				</div>
				<ul class="nav navbar-nav pull-right">
					<li><a><h4><?php echo $username; ?></h4></a></li>
				</ul>
			</div>
		</nav>
		
		<!-- information table goes here -->
		<table class="table table-striped table-bordered">
			<th width="20%">Serial</th>
			<th width="20%">Username</th>
			<th width="20%">Email</th>
			<th width="20%">Course</th>
			<th width="20%">Action</th>
			
<?php 
	$studentData = $user->getStudentData();
	if ($studentData) {
		foreach ($studentData as $value) {
?>
			<tr>
				<td><?php echo $value['id']; ?></td>
				<td><?php echo $value['username']; ?></td>
				<td><?php echo $value['email']; ?></td>
				<td><?php echo $value['course']; ?></td>
				<td>
					<!--<a class="btn btn-primary" href="profile.php?id=1">View</a>-->
					<!-- Split button -->
					<div class="btn-group">
					  <button type="button" class="btn btn-default">
							<?php
								if ($value['active'] == 0) {
									echo "Pending";
								} elseif($value['active'] == 1) {
									echo "Approved";
								} elseif($value['active'] == 2) {
									echo "Rejected";
								}
							?>
					  </button>
					  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					    <span class="caret"></span>
					    <span class="sr-only">Toggle Dropdown</span>
					  </button>
					  <ul class="dropdown-menu">
					    
					  <?php
					    	if($value['active'] == 2) { ?>
					    		<li><a href="approve.php?action=approve&id=<?php echo $value['id']; ?>" onclick="return confirm('Are you sure you want to approve?');">
					    			<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Approve</a>
					    		</li>
					  <?php	} elseif($value['active'] == 1) { ?>
					   			<li><a href="approve.php?action=reject&id=<?php echo $value['id']; ?>" onclick="return confirm('Are you sure you want to delete data?');">
					    			<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Reject</a>
					    		</li>
					  <?php } elseif($value['active'] == 0) { ?>
					  			<li><a href="approve.php?action=approve&id=<?php echo $value['id']; ?>" onclick="return confirm('Are you sure you want to approve?');">
					    			<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Approve</a>
					    		</li>
					    		<li><a href="approve.php?action=reject&id=<?php echo $value['id']; ?>" onclick="return confirm('Are you sure you want to delete data?');">
					    			<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Reject</a>
					    		</li>
					  <?php } ?>
						<!-- 
					    <li><a href="#">Something else here</a></li>
					    <li role="separator" class="divider"></li>
					    <li><a href="#">Separated link</a></li>
					    -->
					  </ul>
					</div>
				</td>
			</tr>

<?php 	
		}
	}
?>

		</table>

	</div>
</div>

<?php include 'inc/footer.php' ?>
